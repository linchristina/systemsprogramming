/*
 * mm-naive.c - The fastest, least memory-efficient malloc package.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "mm.h"
#include "memlib.h"

/* single word (4) or double word (8) alignment */
#define ALIGNMENT 8

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)


#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

#define BLK_HDR_SIZE ALIGN(sizeof(blockHdr))

#define MIN_BLK_SIZE (BLK_HDR_SIZE + SIZE_T_SIZE)

typedef struct header blockHdr;

struct header {
    size_t size;
    blockHdr *next_p;
    blockHdr *prior_p;
};

blockHdr *find_fit(size_t size);
void place_block(blockHdr *bp, size_t bsize);
void *coalesce(blockHdr *bp);

void print_heap();
int in_explicit_list(blockHdr *bp);
void check_heap();

/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
    size_t init = ALIGN(MIN_BLK_SIZE + SIZE_T_SIZE),    // initial block header, footer and sentinel header
           first = ALIGN(init - SIZE_T_SIZE);
    blockHdr *bp = mem_sbrk(init);
    bp->size = first | 1;
    bp->next_p = bp;
    bp->prior_p = bp;
    *(size_t *)((char *)bp + BLK_HDR_SIZE) = bp->size;
    *(size_t *)((char *)mem_heap_hi() - (SIZE_T_SIZE - 1)) = SIZE_T_SIZE | 1;
    return 0;
}

/* 
 * mm_malloc - Allocate a block by incrementing the brk pointer.
 *     Always allocate a block whose size is a multiple of the alignment.
 */
void *mm_malloc(size_t size)
{
    size_t newsize = ALIGN(SIZE_T_SIZE + size + SIZE_T_SIZE);
    size_t *p;
    blockHdr *bp = find_fit(newsize);
    if (bp == NULL) {
        bp = mem_sbrk(newsize);
        if ((long)bp == -1) {
            return NULL;
        }
        else {
            p = (size_t *)((char *)bp - SIZE_T_SIZE);
            *p = newsize | 1;
            *(size_t *)((char *)p + newsize - SIZE_T_SIZE) = *p;
            *(size_t *)((char *)mem_heap_hi() - (SIZE_T_SIZE - 1)) = SIZE_T_SIZE | 1;
            bp = (blockHdr *)p;
        }
    }
    else {
        place_block(bp, newsize);
    }
    return (char *)bp + SIZE_T_SIZE;
}

/*
 * mm_free - Freeing a block does nothing.
 */
void mm_free(void *ptr)
{
    blockHdr *bp = ptr - SIZE_T_SIZE,
             *head = mem_heap_lo();

    // free the block
    bp->size &= ~1;
    *(size_t *)((char *)bp + (*(size_t *)bp & ~1) - SIZE_T_SIZE) = bp->size;

    // add freed block to free list after head
    bp->next_p = head->next_p;
    bp->prior_p = head;
    head->next_p = bp;
    bp->next_p->prior_p = bp;
    
    // coalesce
    coalesce(bp);
}

/*
 * mm_realloc - Implemented simply in terms of mm_malloc and mm_free
 */
void *mm_realloc(void *ptr, size_t size)
{
    return NULL;
}



blockHdr *find_fit(size_t size) {
    blockHdr *p;
    for (p = ((blockHdr *)mem_heap_lo())->next_p;
         p != mem_heap_lo() && p->size < size;
         p = p->next_p);

    if (p != mem_heap_lo())
        return p;
    else
        return NULL;
}

void place_block(blockHdr *bp, size_t bsize) {
    int old_size = bp->size,
        rem_size = old_size - bsize;
    blockHdr *np;

    if (rem_size >= MIN_BLK_SIZE) {
        *(size_t *)bp = bsize | 1;
        *(size_t *)((char *)bp + (*(size_t *)bp & ~1) - SIZE_T_SIZE) = *(size_t *)bp;
        np = (blockHdr *)((char *)bp + bsize);
        np->size = rem_size;
        np->next_p = bp->next_p;
        np->prior_p = bp->prior_p;
        np->prior_p->next_p = np->next_p->prior_p = np;
    }
    else {
        bp->size |= 1;
        *((size_t *)((char *)bp + (bp->size & ~1) - SIZE_T_SIZE)) = bp->size;
        bp->prior_p->next_p = bp->next_p;
        bp->next_p->prior_p = bp->prior_p;
    }
}

void *coalesce(blockHdr *bp) {
    blockHdr *nextblk, *prevblk;
    size_t *next, *prev;
    int next_alloc, prev_alloc;

    if ((blockHdr *)mem_heap_lo() < bp) {
        prev = (size_t *)((char *)bp - (*(size_t *)((char *)bp - SIZE_T_SIZE) & ~1L));
        prev_alloc = *prev & 1;
        prevblk = (blockHdr *)prev;
    }
    else {
        prev_alloc = 1;
    }
    if (bp < (blockHdr *)mem_heap_hi()) {
        next = (size_t *)((char *)bp + (*((size_t *)bp) & ~1L));
        next_alloc = *next & 1;
        nextblk = (blockHdr *)next;
    }
    else {
        next_alloc = 1;
    }

    if (prev_alloc && next_alloc) {
        return bp;
    }
    else if (prev_alloc && !next_alloc) {
        bp->size += nextblk->size;
        *(size_t *)((char *)bp + bp->size - SIZE_T_SIZE) = bp->size;
        bp->next_p = nextblk->next_p;
        nextblk->next_p->prior_p = bp;
        return bp;
    }
    else if (!prev_alloc && next_alloc) {
        prevblk->size += bp->size;
        *(size_t *)((char *)prevblk + prevblk->size - SIZE_T_SIZE) = prevblk->size;
        prevblk->next_p = bp->next_p;
        bp->next_p->prior_p = prevblk;
        return prevblk;
    }
    else {
        prevblk->size += bp->size + nextblk->size;
        *(size_t *)((char *)prevblk + prevblk->size - SIZE_T_SIZE) = prevblk->size;
        prevblk->next_p = nextblk->next_p;
        nextblk->next_p->prior_p = prevblk;
        return prevblk;
    }
    return bp;
}

void print_heap() {
    blockHdr *bp = mem_heap_lo();
    while (bp < (blockHdr *)mem_heap_hi()) {
        printf("%s block at %p, size %d\n",
                (bp->size&1) ? "allocated" : "free",
                bp,
                (int)(bp->size & ~1));
        bp = (blockHdr *)((char *)bp + (bp->size & ~1));
    }
}

int in_explicit_list(blockHdr *bp) {
    blockHdr *exbp = mem_heap_lo();
    do {
        if (bp == exbp)
            return 1;
        exbp = exbp->next_p;
    } while (exbp != mem_heap_lo());
    return 0;
}

void check_heap() {
    blockHdr *bp = mem_heap_lo();
    int last_free = 0;

    while (bp < (blockHdr *)mem_heap_hi()) {
        if (!(bp->size & 1) && !in_explicit_list(bp)) {
            // problem; free block NOT in explicit list
            printf("free block not in explicit list!\n");
            break;
        }
        if (!(bp->size & 1)) {
            if (last_free) {
                // problem; adjacent free blocks
                printf("adjacent free blocks\n");
                break;
            }
            else {
                last_free = 1;
            }
        }
        else {
            last_free = 0;
        }
        bp = (blockHdr *)((char *)bp + (bp->size & ~1));
    }
}
