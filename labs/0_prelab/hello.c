/* 
 * hello.c - the quintessential hello world program
 */
#include <stdio.h>

int main () {
  printf("1. Christina Lin\n");
  printf("2. A20252062\n");
  printf("3. Why do programmers always mix up Christmas and Halloween: because Dec 25 == Oct 31\n");
  return 0;
}
