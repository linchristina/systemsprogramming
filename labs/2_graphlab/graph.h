/* forward declarations so self-referential structures are simpler */
typedef struct vertex vertex_t;
typedef struct adj_vertex adj_vertex_t;
typedef struct stackitem stackitem_t;
typedef struct stack stack_t;

struct vertex {
	char *name;
	adj_vertex_t *adj_list;
	vertex_t *next;
	int visited;
};

struct adj_vertex {
	vertex_t *vertex;
	int edge_weight;
	adj_vertex_t *next;	
};

struct stackitem {
	vertex_t *vertex;
	stackitem_t *above;
	stackitem_t *below;
};

struct stack {
	stackitem_t *top;
	stackitem_t *bottom;
};

/* Allocates and returns a pointer to a new vertex initialized with 'name'
 * and next */
vertex_t *make_vertex(char *name, vertex_t *next);

/* Searches for a vertex with 'name' in the list of vertexes pointed to by
 * 'head'; returns a pointer to it or NULL if no such vertex is found. */
vertex_t *find_vertex(vertex_t *head, char *name);

/* Allocates and adds an adj_vertex_t initialized with 'to' and 'weight' to
 * the list of adjacent vertexes pointed to by 'head'; potentially modifies
 * the caller's 'head'. */
void add_adj_vertex(adj_vertex_t **head, vertex_t *to, int weight);

/* This is the one function you really should implement as part of your 
 * graph data structure's public API. 
 *
 * `add_edge` adds the specified edge to the graph passed in via the first
 * argument. If either of the edge's vertices are not already in the graph,
 * they are added before their adjacency lists are updated. If the graph
 * is currently empty (i.e., *vtxhead == NULL), a new graph is created,
 * and the caller's vtxhead pointer is modified. 
 *
 * `vtxhead`: the pointer to the graph (more specifically, the head of the
 *            list of vertex_t structures)
 * `v1_name`: the name of the first vertex of the edge to add
 * `v2_name`: the name of the second vertex of the edge to add
 * `weight` : the weight of the edge to add
 */
void add_edge (vertex_t **vtxhead, char *v1_name, char *v2_name, int weight);


/* print adjacency list */
void print_adjlist(vertex_t **vtxhead);
/* returns number of nodes in 'head' */
int count_node(vertex_t **head);


/* stack operations */
void init(stack_t *s);
void push(stack_t *s, vertex_t *vtx);
void pop(stack_t *s);
vertex_t *top(stack_t *s);
int isEmpty(stack_t *s);

/* tour path and length functions */
int find_path_length(vertex_t **vtxhead, char *v1_name, char *v2_name);
void get_tour(vertex_t *start, vertex_t **tour);
int find_tour(vertex_t **vlist_head, vertex_t **tour);

void free_adj_list(vertex_t *v);
void free_list(vertex_t **vtxhead);
