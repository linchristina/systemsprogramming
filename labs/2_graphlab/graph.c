#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graph.h"

/* implement the functions you declare in graph.h here */

vertex_t *make_vertex(char *name, vertex_t *next) {
	vertex_t *p = malloc(sizeof(vertex_t));
	p->name = name;
	p->adj_list = NULL;
	p->next = next;
	p->visited = 0;
	return p;
}

vertex_t *find_vertex(vertex_t *head, char *name) {
	vertex_t *p;
	for (p = head; p != NULL; p = p->next) {
		if (strcmp(p->name, name) == 0)
			return p;
	}
	return NULL;
}

void add_adj_vertex(adj_vertex_t **head, vertex_t *to, int weight) {
	adj_vertex_t *p = malloc(sizeof(adj_vertex_t));
	p->next = *head;
	p->vertex = to;
	p->edge_weight = weight;
	*head = p;
}

void add_edge (vertex_t **vtxhead, char *v1_name, char *v2_name, int weight) {
	vertex_t *addv1, *addv2;
	if (*vtxhead == NULL) {
		*vtxhead = make_vertex(v1_name, *vtxhead);
		*vtxhead = make_vertex(v2_name, *vtxhead);
	}
	else if (!find_vertex(*vtxhead, v1_name)) {
		*vtxhead = make_vertex(v1_name, *vtxhead);
	}
	else if (!find_vertex(*vtxhead, v2_name)) {
		*vtxhead = make_vertex(v2_name, *vtxhead);
	}

	addv1 = find_vertex(*vtxhead, v1_name);
	addv2 = find_vertex(*vtxhead, v2_name);
	add_adj_vertex(&addv1->adj_list, addv2, weight);
	add_adj_vertex(&addv2->adj_list, addv1, weight);
}


void print_adjlist(vertex_t **vtxhead) {
	vertex_t *vp;
	adj_vertex_t *adjv;
	for (vp = *vtxhead; vp != NULL; vp = vp->next) {
		printf("  %s: ", vp->name);
		for (adjv = vp->adj_list; adjv != NULL; adjv = adjv->next) {
			printf("%s(%d) ", adjv->vertex->name, adjv->edge_weight);
		}
		printf("\n");
	}
}

int count_node(vertex_t **head) {
	vertex_t *p;
	int count = 0;
	for (p = *head; p != NULL; p = p->next)
		count++;

	return count;
}


void init(stack_t *s) {
	s->top = NULL;
	s->bottom = NULL;
}

void push(stack_t *s, vertex_t *vtx) {
	stackitem_t *newitem;
	newitem = malloc(sizeof(stackitem_t));
	newitem->vertex = vtx;
	newitem->above = NULL;
	newitem->below = NULL;
	if (s->top == NULL) {
		s->top = newitem;
		s->bottom = newitem;
	}
	else {
		newitem->below = s->top;
		s->top = newitem;
	}
}

void pop(stack_t *s) {
	stackitem_t *item;
	if (s->top == NULL) {
		// no items in stack
		return;
	}
	else {
		item = s->top;
		s->top = item->below;
		if (s->top != NULL)
			s->top->above = NULL;
		free(item);
	}
}

vertex_t *top(stack_t *s) {
	return s->top->vertex;
}

int isEmpty(stack_t *s) {
	return (s->top == NULL ? 1 : 0);
}


int find_path_length(vertex_t **vtxhead, char *v1_name, char *v2_name) {
	vertex_t *vp1, *vp2;
	adj_vertex_t *adjv;
	int length = 0;

	vp1 = find_vertex(*vtxhead, v1_name);
	vp2 = find_vertex(*vtxhead, v2_name);

	for (adjv = vp1->adj_list; adjv != NULL; adjv = adjv->next) {
		if (strcmp(adjv->vertex->name, vp2->name) == 0)
			return length + adjv->edge_weight;
	}
	return length;
}

void get_tour(vertex_t *start, vertex_t **tour) {
	stack_t stack;
	vertex_t *vtx = start, *vp;
	adj_vertex_t *adjv;

	// initialize stack and push initial vertex onto stack
	init(&stack);
	push(&stack, vtx);
	// do until stack is empty
	while (!isEmpty(&stack)) {
		// get vertex at top of stack and push off of stack
		vp = top(&stack);
		pop(&stack);
		// if vertex hasn't been visited, visit it and add to tour
		if (vp->visited == 0) {
			vp->visited = 1;
			*tour = make_vertex(vp->name, *tour);
		}
		// repeat for adjacent vertices
		for (adjv = vp->adj_list; adjv != NULL; adjv = adjv->next) {
		if (adjv->vertex->visited == 0)
			push(&stack, adjv->vertex);
		}
	}
}

int find_tour(vertex_t **vlist_head, vertex_t **tour) {
	vertex_t *vtx = *vlist_head, *vp;
	int length = 0;

	// find tour starting from first vertex vlist_head
	get_tour(vtx, tour);
	// calculate total length of tour
	for (vp = *tour; vp->next != NULL; vp = vp->next) {
		length = length + find_path_length(vlist_head, vp->name, vp->next->name);
	}

	return length;
}


void free_adj_list(vertex_t *v) {
	adj_vertex_t *adjv, *tmp;
	adjv = v->adj_list;
	while (adjv != NULL) {
		tmp = adjv->next;
		free(adjv);
		adjv = tmp;
	}
}

void free_list(vertex_t **vtxhead) {
	vertex_t *vp = *vtxhead, *tmp;
	while (vp != NULL) {
		tmp = vp->next;
		free_adj_list(vp);
		free(vp);
		vp = tmp;
	}
}
