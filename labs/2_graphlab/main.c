#include <stdlib.h>
#include <stdio.h>
#include "graph.h"

int main (int argc, char *argv[]) {

	vertex_t *vlist_head = NULL, *vp, *tour = NULL, *tmp;
	int length;
	int i;
	for (i = 1; i < argc; i = i+3) {
		char *arg1 = (char *)argv[i];
		char *arg2 = (char *)argv[i+1];
		int arg3 = atoi(argv[i+2]);
		add_edge(&vlist_head, arg1, arg2, arg3);
	}
/*
	add_edge(&vlist_head, "Chicago", "Plainfield", 30);
	add_edge(&vlist_head, "Chicago", "OakPark", 8);
	add_edge(&vlist_head, "OakPark", "Evanston", 14);
	add_edge(&vlist_head, "Evanston", "Schaumburg", 24);
	add_edge(&vlist_head, "Schaumburg", "Chicago", 30);
	add_edge(&vlist_head, "Evanston", "Chicago", 12);
*/

	printf("Adjacency list:\n");
	print_adjlist(&vlist_head);
	printf("\n");

	length = find_tour(&vlist_head, &tour);
	printf("Tour path:\n");
	if (count_node(&tour) < count_node(&vlist_head)) {
		printf(" No tour exists starting at %s", tour->name);
		length = 0;
	}
	else {
		for (vp = tour; vp != NULL; vp = vp->next)
			printf(" %s", vp->name);
	}
	printf("\n\n");

	printf("Tour length: %d\n", length);

	free_list(&vlist_head);
	vp = tour;
	while (vp != NULL) {
		tmp = vp->next;
		free(vp);
		vp = tmp;
	}

	return 0;
}
