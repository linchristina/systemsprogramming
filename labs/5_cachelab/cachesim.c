#include <stdio.h>
#include <stdlib.h>

typedef struct cache_line cache_line_t;
typedef struct cache_set cache_set_t;
typedef struct cache cache_t;

struct cache_line {
    long tag;
    int valid;
    long timestamp;
};

struct cache_set {
    cache_line_t *lines;
};

struct cache {
    int associativity;
    int num_lines;
    int bytes_per_block;
    struct cache_set *sets;
};

cache_t *make_cache(int associativity, int num_lines, int bytes_per_block) {
    cache_t *cache;
    cache_set_t *set;
    cache_line_t *line;
    int num_sets = num_lines/associativity, i, j;

    cache = malloc(sizeof(cache_t));
    cache->associativity = associativity;
    cache->num_lines = num_lines;
    cache->bytes_per_block = bytes_per_block;
    cache->sets = set = malloc(sizeof(cache_set_t) * num_sets);

    for (i=0; i<num_sets; i++) {
        set[i].lines = line = malloc(sizeof(cache_line_t) * num_lines);
        for (j=0; j<associativity; j++) {
            line[j].tag = 0;
            line[j].valid = 0;
            line[j].timestamp = 0;
        }
    }

    return cache;
}

int main (int argc, char *argv[]) {
    int num_lines = atoi(argv[1]),
        lines_per_set = atoi(argv[2]),
        bytes_per_block = atoi(argv[3]);

    int num_sets = num_lines/lines_per_set,
        hits = 0, misses = 0, requests = 0,
        timestamp = 0, lru = 0, set_idx, tag, i,
        full, emptyline, replaceline, pos, found;

    cache_t *cache = make_cache(lines_per_set, num_lines, bytes_per_block);
    cache_set_t *set = cache->sets;

    char line[80];

    long addr_req;

    printf("Simulating cache with:\n");
    printf(" - Total lines   = %d\n", num_lines);
    printf(" - Lines per set = %d\n", lines_per_set);
    printf(" - Block size    = %d bytes\n", bytes_per_block);
    
    while (fgets(line, 80, stdin)) {
        addr_req = strtol(line, NULL, 0);

        set_idx = (addr_req / bytes_per_block) % num_sets;
        tag = addr_req / (num_sets * bytes_per_block);
        set = cache->sets + set_idx;
        found = 0, full = 1;

        lru = (set->lines[0].timestamp), replaceline = 0;
        for (i=0; i<lines_per_set; i++) {
            // find least recently used
            if (lru > (set->lines[i].timestamp)) {
                lru = (set->lines[i].timestamp);
                replaceline = i;
            }
            // check if set is full or not
            if (set->lines[i].valid == 0)
                full = 0;
            // check if address is already in cache
            else if ((set->lines[i].valid == 1) && (set->lines[i].tag == tag)) {
                found = 1;
                pos = i;
            }
        }
        // find empty line to put address into
        for (i=lines_per_set; i>=0; i--) {
            if (set->lines[i].valid == 0)
                emptyline = i;
        }
        timestamp++;
        requests++;
        // if address is found in cache, increment hits and update timestamp
        if (found) {
            hits++;
            set->lines[pos].timestamp = timestamp;
        }
        // if cache is full, choose a location to replace
        else if (full) {
            misses++;
            set->lines[replaceline].tag = tag;
            set->lines[replaceline].valid = 1;
            set->lines[replaceline].timestamp = timestamp;
        }
        // if address is not found and cache is not full, choose the next empty location to place the address
        else {
            misses++;
            set->lines[emptyline].tag = tag;
            set->lines[emptyline].valid = 1;
            set->lines[emptyline].timestamp = timestamp;
        }
    }
    
    printf("Hit rate %0.2f%% | Miss rate: %0.2f%%\n", ((double)hits/requests) * 100.0, ((double)misses/requests) * 100.0);

    return 0;
}
